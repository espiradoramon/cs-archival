import unittest
from google.cloud import bigquery
from google.cloud import storage
from google.cloud.bigquery import SchemaField

class TestBigQueryIntegration(unittest.TestCase):
    def setUp(self):
        self.bq_client = bigquery.Client()
        self.bucket_name = 'test-cs-up'
        self.bucket = storage.Client().get_bucket(self.bucket_name)
        self.dataset_id = 'test'
        self.table_id = 'test-cs'

    def test_check_file_in_bucket(self):
        file_list = [blob.name for blob in self.bucket.list_blobs()]
        self.assertIn('jan.csv', file_list)


    def test_check_dataset_in_bq(self):
        datasets = list(self.bq_client.list_datasets())
        dataset_ids = [dataset.dataset_id for dataset in datasets]
        self.assertIn(self.dataset_id, dataset_ids)

if __name__ == '__main__':
    unittest.main()
