import unittest

class TestSolution(unittest.TestCase):
    def test_load_csv_from_drive(self):
        # Test that the function correctly loads the CSV file from Google Drive
        result = load_csv_from_drive('test_file.csv')
        expected_result = [
            {'employee_id': '1', 'employee_name': 'John Doe', 'employee_site': 'Site 1', 'employee_team': 'Team A', 'employee_tier': 'Tier 1'},
            {'employee_id': '2', 'employee_name': 'Jane Doe', 'employee_site': 'Site 2', 'employee_team': 'Team B', 'employee_tier': 'Tier 2'},
        ]
        self.assertEqual(result, expected_result)
        
    def test_load_data_into_bigquery(self):
        # Test that the function correctly inserts the data into BigQuery
        data = [
            {'employee_id': '1', 'employee_name': 'John Doe', 'employee_site': 'Site 1', 'employee_team': 'Team A', 'employee_tier': 'Tier 1'},
            {'employee_id': '2', 'employee_name': 'Jane Doe', 'employee_site': 'Site 2', 'employee_team': 'Team B', 'employee_tier': 'Tier 2'},
        ]
        result = load_data_into_bigquery(data, 'test_table')
        self.assertTrue(result)
        
    def test_drop_bigquery_table(self):
        # Test that the function correctly drops the BigQuery table
        result = drop_bigquery_table('test_table')
        self.assertTrue(result)
        
    def test_consolidate_data(self):
        # Test that the function correctly consolidates the data from the current and previous months
        result = consolidate_data('current_month_table', 'consolidated_data_table')
        expected_result = [
            {'employee_id': '1', 'employee_name': 'John Doe', 'employee_site': 'Site 1', 'employee_team': 'Team A', 'employee_tier': 'Tier 1', 'month': 'Jan'},
            {'employee_id': '2', 'employee_name': 'Jane Doe', 'employee_site': 'Site 2', 'employee_team': 'Team B', 'employee_tier': 'Tier 2', 'month': 'Jan'},
            {'employee_id': '3', 'employee_name': 'Jim Doe', 'employee_site': 'Site 3', 'employee_team': 'Team C', 'employee_tier': 'Tier 3', 'month': 'Feb'},
            {'employee_id': '4', 'employee_name': 'Jane Doe', 'employee_site': 'Site 4', 'employee_team': 'Team D', 'employee_tier': 'Tier 4', 'month': 'Feb'},
        ]
        self.assertEqual(result, expected_result)

if __name__ == "__main__":
    unittest.main()