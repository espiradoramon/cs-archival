import csv
import io
import sys
from googleapiclient.discovery import build
from google.auth.transport.requests import AuthorizedSession

def transfer_data(request):
    # Authenticate and build the Google Drive API client
    drive_service = build("drive", "v3", credentials=credentials)

    # Get the CSV file from Google Drive
    file_id = "1ADDxYN87Eer_RgSCM-hJn6lFrYgNWzNn"
    request = drive_service.files().get_media(fileId=file_id)
    file = io.StringIO(request.execute().decode("utf-8"))

    # Connect to BigQuery
    client = bigquery.Client()

    # Create a new table in BigQuery for the current month's data
    table_id = "ornate-producer-230307.test.Month_8" # Replace this with the actual table name
    schema = [
        bigquery.SchemaField("employee_id", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("employee_name", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("employee_site", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("employee_team", "STRING", mode="NULLABLE"),
        bigquery.SchemaField("employee_tier", "STRING", mode="NULLABLE"),
        # Add the rest of the columns here with the appropriate data type and mode
    ]
    table = bigquery.Table(table_id, schema=schema)
    table = client.create_table(table)

    # Load the data from the CSV file into the new table in BigQuery
    with file:
        reader = csv.reader(file)
        headers = next(reader)
        rows = [row for row in reader]

    errors = client.insert_rows(table, rows)
    if errors == []:
        print("Loaded data into table {}".format(table_id))
    else:
        print("Errors: {}".format(errors))

    # Insert the data from the new table into the consolidated previous months data table
    query = """
        INSERT INTO
 `ornate-producer-230307.test.test-cs`
SELECT
 employee_id,
 employee_name,
 employee_site,
 employee_team,
 employee_tier,
 verint_organization,
 designation,
 start_date,
 tool,
 tool_id,
 contact_type,
 queue_language,
 queue_skill,
 segments,
 queue,
 year,
 month,
 week,
 day,
 PARSE_DATE('%m/%d/%Y', date) AS date,
 hourly_interval,
 handled,
 abandoned,
 outbound,
 accept_transferred_out,
 handle_time,
 wait_time,
 abandon_time,
 outbound_time,
 time,
 contact_id,
 originator,
 next_address,
 activity_code,
 cfr_username,
 cfr_customer_verified,
 cfr_product,
 cfr_inquiry,
 cfr_sub_inquiry,
 cfr_ip,
 cfr_country,
 cfr_source,
 cfr_referrer,
 cfr_restart,
 cfr_bet_slip_callback_id,
 cfr_custom_data,
 brand,
 product,
 query_type,
 handled_excl_email,
 interval_thirty,
 topic_name,
 key_word1,
 key_word2,
 key_word3,
 sata_usage,
 daily_staffed_time,
 hourly_staffed_time,
 sla_ans,
 sla_abn,
 en_row_filter,
 blank1,
 social_app,
 queue_tier,
 interval_five,
 country_look_up,
 route_point,
 application_name
FROM
 `satacs-be-dwh-prod.cs_volume_reports.Month_8`  # Here you put the name of the table that you created for the last month data
WHERE date != "Date"
    """
    query_job = client.query(query)
    result = query_job.result()
    print("Inserted data from table {} into consolidated previous months data table".format(table_id))

    # Delete the new table
    client.delete_table(table)
    print("Deleted table {}".format(table_id))
